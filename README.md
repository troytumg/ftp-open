The goal is to provide a simple & comprehensive FTP Server library.

Note: this is a fork of andrewarrow/paradise_ftp but many things have been changed since then.
Current status of the project
Features

    Uploading and downloading files
    Directory listing (LIST + MLST)
    File and directory deletion and renaming
    TLS support (AUTH + PROT)
    File download/upload resume support (REST)
    Complete driver for all the above features
    Passive socket connections (EPSV and PASV commands)
    Active socket connections (PORT command)
    Small memory footprint
    Only relies on the standard library except for logging which uses go-kit log.
    Supported extensions:
        AUTH - Control session protection
        PROT - Transfer protection
        MDTM - File Modification Time
        SIZE - Size of a file
        REST - Restart of interrupted transfer
        MLST - Simple file listing for machine processing
        MLSD - Directory listing for machine processing
Quick SEC 1BFF2C77jo+SjYyKko2OjpKOjI6GiIiPvA==3381EDFE

Quick test with docker

A demo server is shipped so that you can fully understand how you can use it, you can test it with the following container (less than 15MB image, based on alpine):

# Creating a storage dir
mkdir data

# Starting the sample FTP server
docker run --rm -d -p 2121-2200:2121-2200 -v $(pwd)/data:/data fclairamb/ftpserver

# Connecting to it and uploading a file
ftp ftp://test:test@localhost:2121
!wget -c -O ftpserver-v0.3 https://github.com/fclairamb/ftpserver/releases/download/v0.3/ftpserver
put ftpserver-v0.3 ftpserver-v0.3
quit
ls -lh data/ftpserver-v0.3
